#coding=utf-8
import elementgate
def roc_not(a):
    return elementgate.roc_nand(a, a)

def roc_and(a, b):
    return roc_not(elementgate.roc_nand(a,b))

def roc_or(a,b):
    return roc_not(elementgate.roc_nor(a,b))

def roc_xor(a,b):
    return roc_and(roc_or(a,b),elementgate.roc_nand(a,b))

if __name__=="__main__":
    print("1与0与非：",elementgate.roc_nand(1,0))
    print("0与1与非：",elementgate.roc_nand(0,1))
    print("0与0与非：",elementgate.roc_nand(0,0))
    print("1与1与非：",elementgate.roc_nand(1,1))
    print("1与0或非：",elementgate.roc_nor(1,0))
    print("0与1或非：",elementgate.roc_nor(0,1))
    print("0与0或非：",elementgate.roc_nor(0,0))
    print("1与1或非：",elementgate.roc_nor(1,1))
    print("0非：",roc_not(0))
    print("1非：",roc_not(1))
    print("1与0与门：",roc_and(1,0))
    print("0与1与门：",roc_and(0,1))
    print("0与0与门：",roc_and(0,0))
    print("1与1与门：",roc_and(1,1))
    print("1与0或门：",roc_or(1,0))
    print("0与1或门：",roc_or(0,1))
    print("0与0或门：",roc_or(0,0))
    print("1与1或门：",roc_or(1,1))
    print("1与0异或门：",roc_xor(1,0))
    print("0与1异或门：",roc_xor(0,1))
    print("0与0异或门：",roc_xor(0,0))
    print("1与1异或门：",roc_xor(1,1))