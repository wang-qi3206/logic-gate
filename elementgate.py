#coding=utf-8
def roc_nor(a, b):
    return  int(not(a or b))

def roc_nand(a, b):
    return  int(not(a and b))

if __name__ == "__main__":
    print("1与0与非：",roc_nand(1,0))
    print("0与1与非：",roc_nand(0,1))
    print("0与0与非：",roc_nand(0,0))
    print("1与1与非：",roc_nand(1,1))
    print("1与0或非：",roc_nor(1,0))
    print("0与1或非：",roc_nor(0,1))
    print("0与0或非：",roc_nor(0,0))
    print("1与1或非：",roc_nor(1,1))