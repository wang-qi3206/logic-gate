import elementgate
import basicgate

class fulladder:
      def sum(a,b,c):
            return basicgate.roc_xor(basicgate.roc_xor(a,b),c)
      def carry(a,b,c):
            return basicgate.roc_not(basicgate.roc_and(basicgate.roc_and(elementgate.roc_nand(a,b),elementgate.roc_nand(a,c)),elementgate.roc_nand(b,c)))

if __name__ == "__main__":
    print("Ai       Bi       C(i-1)       Si        Ci")
    print("0        0          0          {0}         {1} ".format(fulladder.sum(0,0,0),fulladder.carry(0,0,0)))
    print("0        0          1          {0}         {1} ".format(fulladder.sum(0,0,1),fulladder.carry(0,0,1)))
    print("0        1          0          {0}         {1} ".format(fulladder.sum(0,1,0),fulladder.carry(0,1,0)))
    print("0        1          1          {0}         {1} ".format(fulladder.sum(0,1,1),fulladder.carry(0,1,1)))
    print("1        0          0          {0}         {1} ".format(fulladder.sum(1,0,0),fulladder.carry(1,0,0)))
    print("1        0          1          {0}         {1} ".format(fulladder.sum(1,0,1),fulladder.carry(1,0,1)))
    print("1        1          0          {0}         {1} ".format(fulladder.sum(1,1,0),fulladder.carry(1,1,0)))
    print("1        1          1          {0}         {1} ".format(fulladder.sum(1,1,1),fulladder.carry(1,1,1)))