import basicgate

class halfadder:
    def sum(a,b):
        return basicgate.roc_xor(a,b)
    def carry(a,b):
        return basicgate.roc_and(a,b)

if __name__ == "__main__":
    print("A         B         S        C")
    print("0         0         {0}        {1}".format(halfadder.sum(0,0),halfadder.carry(0,0)))
    print("0         1         {0}        {1}".format(halfadder.sum(0,1),halfadder.carry(0,1)))
    print("1         0         {0}        {1}".format(halfadder.sum(1,0),halfadder.carry(1,0)))
    print("1         1         {0}        {1}".format(halfadder.sum(1,1),halfadder.carry(1,1)))