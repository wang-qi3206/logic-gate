import elementgate
import basicgate

def vote(a,b,c):
    return basicgate.roc_not(basicgate.roc_and(basicgate.roc_and(elementgate.roc_nand(a,b),elementgate.roc_nand(a,c)),elementgate.roc_nand(b,c)))

if __name__ == '__main__':
    print("A           B          C          F")
    print("0           0          0          {}".format(vote(0,0,0)))
    print("0           0          1          {}".format(vote(0,0,1)))
    print("0           1          0          {}".format(vote(0,1,0)))
    print("0           1          1          {}".format(vote(0,1,1)))
    print("1           0          0          {}".format(vote(1,0,0)))
    print("1           0          1          {}".format(vote(1,0,1)))
    print("1           1          1          {}".format(vote(1,1,1)))