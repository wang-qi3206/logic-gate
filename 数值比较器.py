import basicgate

a = int(input("请输入一个三位二进制数："))
b = int(input("请再输入一个三位二进制数："))
a3 = a // 100
a2 = (a-a3*100) // 10
a1 = a % 100
b3 = b // 100
b2 = (b-b3*100) // 10
b1 = b % 100


x = basicgate.roc_or(basicgate.roc_and(basicgate.roc_not(a3),basicgate.roc_not(b3)),basicgate.roc_and(a3,b3))
y = basicgate.roc_or(basicgate.roc_and(basicgate.roc_not(a2),basicgate.roc_not(b2)),basicgate.roc_and(a2,b2))
z = basicgate.roc_or(basicgate.roc_and(basicgate.roc_not(a1),basicgate.roc_not(b1)),basicgate.roc_and(a1,b1))
p = basicgate.roc_and(basicgate.roc_and(x,y),z)
print("比较结果为:{}(0表示不同，1表示相同)".format(p))